let dot = {x: 0, y: 0};
let apple = {x: 0, y: 0};
let direction = {x: 0, y: 0};
let snake = [dot];
let gameBoard = document.getElementById('game-board');

function createDot(x, y, color) {
    let dot = document.createElement('div');
    dot.style.left = `${x}px`;
    dot.style.top = `${y}px`;
    dot.classList.add(color);
    gameBoard.appendChild(dot);
    return dot;
}

function updateGame() {
    let newDot = {x: snake[0].x + direction.x, y: snake[0].y + direction.y};
    if (newDot.x < 0 || newDot.y < 0 || newDot.x > 380 || newDot.y > 380) {
        return gameOver();
    }
    snake.unshift(newDot);
    if (newDot.x === apple.x && newDot.y === apple.y) {
        apple = {x: Math.random() * 380, y: Math.random() * 380};
        createDot(apple.x, apple.y, 'apple');
    } else {
        gameBoard.removeChild(snake.pop());
    }
    createDot(newDot.x, newDot.y, 'dot');
}

function gameOver() {
    clearInterval(gameInterval);
    alert('Game Over!');
}

let gameInterval = setInterval(updateGame, 200);